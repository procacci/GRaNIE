GRaNIE: Inference of enhancer-mediated gene regulatory networks
=================================================================


Repository structure
----------------------
This repository for our *GRaNIE* package (**G**ene **R**egul**a**tory **N**etwork **I**nference including **E**nhancers) contains everything that is related to this project.


All relevant information related to the package, including installation instructions, a detailed documentation and how to cite us, is available at the following stable URL and is regularly updated and extended:

**[https://grp-zaugg.embl-community.io/GRaNIE](https://grp-zaugg.embl-community.io/GRaNIE)**
