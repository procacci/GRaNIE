# GRaNIEverse 0.1.2 (2023-05-08)

- added the first function to compare eGRNS: `compareConnections_upsetPlots`. This function takes a list of GRaNIE eGRNs and produces a summary upset plot to compare among all input eGRNs. It can be used for TF-peak, TF-gene, TF-peak-gene and peak-gene pairs.


# GRaNIEverse 0.1.1 (2023-04-25)

- added `corMethod` to the GRaNIE batch function and the wrapper to allow overriding and specifying the correlation method
- fixed a bug in `prepareSeuratData_GRaNIE` that appeared sometimes when using a custom pseudobulk variable as source for clustering. Thanks to Gerard for noticing and fixing!

# GRaNIEverse 0.1 (2023-04-17)

- first package version